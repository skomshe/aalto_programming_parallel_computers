
PROGRAM main

USE omp_lib
USE mod_step_v0
USE mod_step_v0_omp
USE mod_step_v1
USE mod_step_v2

IMPLICIT NONE

INTEGER  (KIND = 4), PARAMETER                                              ::&
  nb = 16, nd = 3
INTEGER  (KIND = 4)                                                         ::&
  i1, j1, n, na, nab            
REAL     (KIND = 4)                                                         ::&
  PI, st, en                  
REAL     (KIND = 4), DIMENSION(:,:), ALLOCATABLE                            ::&
  r, d 

PI = 4.0D0*ATAN(1.0D0)
  
n = 4000
ALLOCATE(r(n,n))
ALLOCATE(d(n,n))

! A very inefficient loop
DO i1 = 1,n
  DO j1 = 1,n
    d(i1,j1) = ABS(SIN(i1*PI/n)*COS(j1*PI/n))
  END DO
END DO

st = OMP_GET_WTIME()

! Basic sequential without concern for column-major storage. 
! For n=4000, time=568.57s
! CALL step_v0(n,d,r)

! Basic sequential with openmp without concern for column-major storage
! For n=4000, time=162.91s
! CALL step_omp(n,d,r) 

! Make transpose with openmp using column-major storage for hardware prefetch
! For n=4000, time=5.67s
! CALL step_v1(n,d,r) 

! Increase throughput with having many statements that can be executed together
! For n=4000, time=6.96s
na = (n + nb - 1)/nb
nab = na*nb
CALL step_v2(n,na,nb,nab,d,r) ! use nb=16, na=250, nab=4000


en = OMP_GET_WTIME()

WRITE(*,'(A,F6.2,A)') 'Time = ', en-st, 'seconds'

! ! When testing, run with n = 5 and uncomment below
! WRITE(*,*) 'Max error = ', MAXVAL(ABS(r(1,:) - &
!   (/ 0.58778525229247325D0, 0.47552825814757682D0, 0.47552825814757671D0,&
!   0.58778525229247325D0, 0.58778525229247325D0 /)))
! WRITE(*,*) 'Max error = ', MAXVAL(ABS(r(2,:) - &
!   (/ 0.95105651629515364D0, 0.58778525229247314D0, 0.58778525229247292D0,&
!   0.95105651629515364D0, 0.95105651629515364D0 /)))
! WRITE(*,*) 'Max error = ', MAXVAL(ABS(r(3,:) - &
!   (/ 0.95105651629515375D0, 0.58778525229247314D0, 0.58778525229247303D0,&
!   0.95105651629515375D0, 0.95105651629515375D0 /)))
! WRITE(*,*) 'Max error = ', MAXVAL(ABS(r(4,:) - &
!   (/ 0.58778525229247336D0, 0.47552825814757682D0, 0.47552825814757671D0,&
!   0.58778525229247336D0, 0.58778525229247336D0 /)))
! WRITE(*,*) 'Max error = ', MAXVAL(ABS(r(5,:)))

DEALLOCATE(r,d)

END PROGRAM main
