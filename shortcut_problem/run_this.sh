#! /bin/bash

echo "Removing previous files ..."
rm -f *.o *.mod *.s main

echo "Compiling ..."
# Add -S for generating assembly code
gfortran -O3 -march=native -fopenmp -c mod_step_v0.f90 
gfortran -O3 -march=native -fopenmp -c mod_step_v0_omp.f90 
gfortran -O3 -march=native -fopenmp -c mod_step_v1.f90 
gfortran -O3 -march=native -fopenmp -c mod_step_v2.f90 

echo "Linking ..."
gfortran -O3 -march=native -fopenmp main.f90 *.o -o main

echo "Running the executable ..."
export OMP_NUM_THREADS=4
./main

