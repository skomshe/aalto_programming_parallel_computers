MODULE mod_step_v2

USE omp_lib

IMPLICIT NONE

CONTAINS
!------------------------------------------------------------------------------

SUBROUTINE step_v2(n,na,nb,nab,d,r)

IMPLICIT NONE

INTEGER  (KIND = 4)                                                         ::&
    n, na, nb, nab
REAL     (KIND = 4), DIMENSION(:,:)                                         ::&
    r, d 

INTEGER  (KIND = 4)                                                         ::&
    i1, j1, k1, kb
REAL     (KIND = 4)                                                         ::&
    z
REAL     (KIND = 4), DIMENSION(nb)                                          ::&
    vv
REAL     (KIND = 4), DIMENSION(:,:), ALLOCATABLE                            ::&
    d_pad, t_pad

ALLOCATE(d_pad(nab,n))
ALLOCATE(t_pad(n,nab))
d_pad(:,:) = 1.0D5
t_pad(:,:) = 1.0D5

!$OMP PARALLEL DO DEFAULT(NONE) &
!$OMP PRIVATE(i1,j1) &
!$OMP SHARED(n,d,d_pad,t_pad)   
DO i1 = 1,n
    DO j1 = 1,n
        d_pad(j1,i1) = d(j1,i1)
        t_pad(j1,i1) = d(i1,j1)
    END DO
END DO
!$OMP END PARALLEL DO


!$OMP PARALLEL DO DEFAULT(NONE) &
!$OMP PRIVATE(i1,j1,k1,kb,z,vv) &
!$OMP SHARED(n,na,nb,r,d_pad,t_pad)        
DO i1 = 1,n
    DO j1 = 1,n

        DO kb = 1,nb
            vv(kb) = t_pad(kb,i1) + d_pad(kb,j1)
        END DO

        DO k1 = 2,na
            DO kb = 1,nb
                z = t_pad((k1-1)*nb+kb,i1) + d_pad((k1-1)*nb+kb,j1)
                vv(kb) = MIN(z,vv(kb))
            END DO
        END DO

        r(i1,j1) = MINVAL(vv)

    END DO
END DO
!$OMP END PARALLEL DO

DEALLOCATE(d_pad,t_pad)

END SUBROUTINE step_v2

END MODULE mod_step_v2